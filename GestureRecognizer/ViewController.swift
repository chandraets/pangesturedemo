//
//  ViewController.swift
//  GestureRecognizer
//
//  Created by Puja Bhagat on 10/12/2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imgFile: UIImageView!
    @IBOutlet weak var imgTrash: UIImageView!
    
    var fileViewOrigin: CGPoint?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addPanGesture(view: imgFile)
        fileViewOrigin = imgFile.frame.origin
        view.bringSubviewToFront(imgFile)
    }

    
    func addPanGesture(view: UIView){
        let pan = UIPanGestureRecognizer(target: self, action: #selector(ViewController.handlePan(sender:)))
        view.addGestureRecognizer(pan)
    }

    @objc func handlePan(sender: UIPanGestureRecognizer) {
        let fileView = sender.view!

        switch sender.state {
            case .began, .changed:
                moveViewWithPan(view: fileView, sender: sender)
            case .ended:
                if fileView.frame.intersects(imgTrash.frame) {
                    deleteView(view: fileView)
                } else {
                    returnViewToOrigin(view: fileView)
                }
            
            default:
                break
        }

    }
    
    func deleteView(view: UIView) {
        UIView.animate(withDuration: 0.3) {
            view.alpha = 0.0
        }
    }
    
    func returnViewToOrigin(view: UIView) {
        UIView.animate(withDuration: 0.3) {
            view.frame.origin = self.fileViewOrigin!
        }
    }
    
    func moveViewWithPan(view: UIView, sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: view)
        view.center = CGPoint(x: (view.center.x) + translation.x, y: (view.center.y) + translation.y)
        sender.setTranslation(CGPoint.zero, in: view)
    }
}

